# ARKit
### [Session 602](https://developer.apple.com/videos/play/wwdc2017/602/)
* Mobile AR Platform
* High-level API
* iOS (A9 and up)

## Layers
### Tracking
* World tracking
* Visual inertial odometry
* No external setup

### Scene Understanding
* Plane detection
* Hit-testing
* Light estimation

### Rendering
* Easy integration
* AR views
* Custom rendering

## How to use ARKit
* Session based API (`ARSession`)
###`ARSessionConfiguration`
* Determines what type of tracking to run
* Properties to enable and disable Features
* Provides availabitity of device capabilities
### `ARSession`
* `run`
* `pause`
    * No tracking occurs after pause is run
* Change configuration by calling `run(newConfig)`
* Reset tracking available
* Implement delegates to get current frame and errors
### `ARFrame`
* Captured Image
* Tracking info
* Scene Info
### `ARAnchor`
* Real-world position and orientation
* `ARSession` add/remove
* `ARFrame anchors`
* `ARSessionDelegate` add/update/remove

## Tracking
### World tracking
* Position and orientation of device
* Provides real world scale as well
* Physical distance is available (device traveled)
* Relative to starting position
* 3D-feature points

### `ARCamera`
* Transform
* Tracking state
* Camera intrinsics

### Quality
* Uninterrupted sensor data
* Textured environments
* Static scenes
* Tracking state used to deal with these
    * Not Available
    * Normal
    * Limited (Provides a reason)
* Delegate informs trackingStateChange

### Session Interruptions
* Camera input unavailable
* Tracking is stopped
* Dealing with interruptions is important
* Handled via delegates

## Scene Understanding
### Plane Detection
* Horizontal with respect to gravity
* Runes over multiple frames
* Aligned extent for surface
* Plane merging
#### `ARPlaneAnchor`
* Subclass of `ARAnchor`
* Transform
* Extent
* Center
* Delegate can handle new anchor being added

### Hit-Testing
* Intersect ray with real world
* Uses scene information
* Results sorted by distance
* Hist-test types (multiple types allowed)
    * Existing plane using extent
    * Existing plane
        * treats plane as infinite
    * Estimated plane 
        * created from co-planar points
    * Feature point 
        * disintiguous points

### Light Estimation
* Ambient intensity based on captured image
* Defaults to 1000 lumen
* Enabled by default
* `ARFrame` has `lightEstimate` property with data attached

## Rendering
### SceneKit
* `ARSCNView`
* Draws captured image
* Updates a `SCNCamera`
* Updates scene ligh
# What's New in Swigning for Xcode and Xcode Server
### [Session 403](https://developer.apple.com/videos/play/wwdc2017/403/)
## Fundamentals
### Why code sign
* Protectes user privacy and secuity
* Authenticates developer
* Guarentees Apple allowed system services
* Certificates, Provision Profiles, Entitlements

#### For more What's New in Xcode App Signing 2016
* Xcode 9 brings Automatic and Manual Signing to Xcode Server

### Xcode server
* CI powered by Xcode
* Xcode 9 brings it built into Xcode
* Runs your tests on simulators and devices
* Join Xcode Server to your team for development signing
* Supports two-factor authentication

### Xcodebuild
* Command line support for automatic signing repairs
    * `--allowProvisionUpdates`
    * `--alowProvisioningSigningUpdates`

## Development Signing with Xcode Server
## Manual signing improvements
* Manage certificates in the accounts preferences
* Preview and download profiles in the project edtior

## Distrubing your app
* Development sign archives
* IPA is distrubtion signed
    * Can be Ad-Hoc or Enterprise
* Streamlined distro workflow
* Improved visibility of errors
* Enhanced export (Save IPA for later if needed)
* DistributionSummary.plist
* Packaging.log
* ExportOptions.plist
    * Allows setting up defaults with what you want for a build
    * Check video

## Distrubtion Signing with Xcode Server
* Needs Distrubion Signing Cert impported manually with auto coding signing
* Copy distribution certs to server
* Auto signging creatses and updates distro profiles
* Upload ExportOptions.plist
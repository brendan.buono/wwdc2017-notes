# Localizing in XCode 9
### [Session 401](https://developer.apple.com/videos/play/wwdc2017/401/)

## Internationalization
### String Management
* Use `NSLocalizedString` to land string in code
* XIBs and Storyboards are automatically fine
* Use `localizedStringWithFormat` to get a localized formatted string
* `label.text = NSLocalizedString(“Pop”,comment:”Label preceding the pop value)`
* `String.localizeStringWithFormat`
* Static Analyzer will find problems in ObjC
## Format
* `let formatter = DateFormatter()`
## Localized Resource Structure
* Auto Layout will help deal with different strings
* Pseudo localization (Internationalize Lorem Ipsum)]
* Pseudo localization can be set in Scheme

## Overall
* Use standard APIs
* Use base internalization and Auto Layout
* Validate your strings and UI

## Xcode Localization workflow
* Source Code -> Resources -> Export to industry standard XML
* XML -> Resources
### Stringdict files supported in Xcode 9
* Handling Plurals
* Adaptive Strings
    * Dealing with truncated strings
    * `VariableWidthRuleType`
    * `string.varientFittingPresentationWidth(20)`
* Other Resources
    * Click on Asset and click Localize


## Testing
* Tests can be run in any language >=9
* USE ACCESSIBILITY IDENTIFIERS
* XCTAttachment to get screenshots

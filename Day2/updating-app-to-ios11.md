# Updating your app to iOS 11
### [Session 204](https://developer.apple.com/videos/play/wwdc2017/204/)
## Adopting new functionality in UIKit
#### `UIBarItem` (superclass to TabBar and NavigationBar)
* `UIBarItem.landscapeImagePhone`
* `UIBarItem.largeContentSizeImage`

#### `NavigationBar`
* `prefersLargeTitle = true` sets large display mode
* `navigationItem.largeTitleDisplayMode` allows for dynamic sizing

#### `UISearchController`
* Placed in `navigationItem.searchController`
* `navigationItem.hidesSearchBarWhenScrolling`

#### `UINavigationController` is responsible for some features
* `UISearchControllers`, refresh controls and rubber banding are not supported if you manually push and pop
#### `UIToolbar` and `UINavigationBar` support auto layout

#### Avoid Zero-Sized Custom Views
* `UINavigationBar` and `UIToolbar` provide position
* Devs must provide size
* Implmenet `intrinsicContentSize`
* Connect your subviews via constraints

## Margins and Insets
### Layout Margins
* `directionalLayoutMargins`
* `systemMinimumLayoutMargins`
### Safe Area
* Describes area of view not occluded by ancestors
* Available as insets or layout guide
## Table View
* Set table view estimated height to 0 to turn off
* Swipe actions from left and right
* `leadingSwipeActionsConfigurationForRowAt` and `trailingSwipeActionsConfigurationForRowAt` for swiping

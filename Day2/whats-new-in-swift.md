# Whats new in Swift
### [Session 402](https://developer.apple.com/videos/play/wwdc2017/402/)
## Code updates
* ` func updatePlayerPicker(views: [UIView & PlayerPicker])`
    * Mix and match classes and protocols to define what can be passed in

## Build Times
* Precompiled headers in XCode 9
* Build process updates the index
* Compiler removes unused Conformance removal
* Minimal inference good for removing unused thunks
* Move `@objc` functions into an extension
* Swift Symbols are removed by default in binaries (archiving mode)
* Swift standard libraries are stripped during App Thinning

## Strings, Collections, and Generics
### Strings
* Grapheme Breaking added to Strings
* Strings are a collection of characters
* follows the standard `Collection` protocol
* Slice type in Swift 4 is `Substring`
     * `Substring` is a reference value
     * Only use `String` in explicit code, `Substring` forces copy when needing to treat it as a String

#### Mult-line String Literal
#### ` """ This is  a multiline string """`
* Indentation follows closing line 

### Generics
* `where` clauses on associated types
* `Collection` supports open ended subscript

## Exclusive access to memory
* Make it easier to reason about local variables
* Enable better programmer optimization
* Enable better compiler optimization
* Enable powerful new langugage features
* Read+Read √
* Read+Write NO
* Write+Write NO
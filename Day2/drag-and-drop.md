# Drag and Drop
### [Session 203](https://developer.apple.com/videos/play/wwdc2017/203/)
 A way to graphically move data from one application to another
## Goals and Concepts
### Responsive
* On demand

### Secure
* Data is only visible to destination
* Source may restrict access

## API Fundamentals
 * Available on the iPad and iPhone
 > iPhone can only drag within same app

 ### Phases
* Lift
* Drag
* Set Down
* Data Tranfer

### Enable a Drag
* Drag is attached to a view
    * Similar to GestureRecgonizer
* DragItem
    * Model object associated to the view

### Enable a drop
* `UIPasteConfiguration` for basic concepts
* `UIDropInteraction` responses to drop event
    * Allows to say yes or no if they want the drop
    * Async delievery of items

![Image](https://photos-3.dropbox.com/t/2/AABAiCq8GXMmk2IDe1Q5QftLg9fO4CpcC03XV1c2HNBiKQ/12/640984787/jpeg/32x32/1/_/1/2/DragAndDrop-1.jpg/EK25-pkFGD8gBygH/VWHks4ywj7skiYEHHDf_PJG1ma3JyfWJ9Gnat5buUeY?size=480x320&size_mode=3 480w, https://photos-3.dropbox.com/t/2/AABAiCq8GXMmk2IDe1Q5QftLg9fO4CpcC03XV1c2HNBiKQ/12/640984787/jpeg/32x32/1/_/1/2/DragAndDrop-1.jpg/EK25-pkFGD8gBygH/VWHks4ywj7skiYEHHDf_PJG1ma3JyfWJ9Gnat5buUeY?size=640x480&size_mode=3 640w, https://photos-3.dropbox.com/t/2/AABAiCq8GXMmk2IDe1Q5QftLg9fO4CpcC03XV1c2HNBiKQ/12/640984787/jpeg/32x32/1/_/1/2/DragAndDrop-1.jpg/EK25-pkFGD8gBygH/VWHks4ywj7skiYEHHDf_PJG1ma3JyfWJ9Gnat5buUeY?size=800x600&size_mode=3 800w, https://photos-3.dropbox.com/t/2/AABAiCq8GXMmk2IDe1Q5QftLg9fO4CpcC03XV1c2HNBiKQ/12/640984787/jpeg/32x32/1/_/1/2/DragAndDrop-1.jpg/EK25-pkFGD8gBygH/VWHks4ywj7skiYEHHDf_PJG1ma3JyfWJ9Gnat5buUeY?size=1024x768&size_mode=3 1024w, https://photos-3.dropbox.com/t/2/AABAiCq8GXMmk2IDe1Q5QftLg9fO4CpcC03XV1c2HNBiKQ/12/640984787/jpeg/32x32/1/_/1/2/DragAndDrop-1.jpg/EK25-pkFGD8gBygH/VWHks4ywj7skiYEHHDf_PJG1ma3JyfWJ9Gnat5buUeY?size=1280x960&size_mode=3 1280w, https://photos-3.dropbox.com/t/2/AABAiCq8GXMmk2IDe1Q5QftLg9fO4CpcC03XV1c2HNBiKQ/12/640984787/jpeg/32x32/1/_/1/2/DragAndDrop-1.jpg/EK25-pkFGD8gBygH/VWHks4ywj7skiYEHHDf_PJG1ma3JyfWJ9Gnat5buUeY?size=1600x1200&size_mode=3 1600w, https://photos-3.dropbox.com/t/2/AABAiCq8GXMmk2IDe1Q5QftLg9fO4CpcC03XV1c2HNBiKQ/12/640984787/jpeg/32x32/1/_/1/2/DragAndDrop-1.jpg/EK25-pkFGD8gBygH/VWHks4ywj7skiYEHHDf_PJG1ma3JyfWJ9Gnat5buUeY?size=2048x1536&size_mode=3 2048w)
## Showing the APIs in Action
* Moves are allows only within an app
    * Must deal with it within the app
* Load objects from session, force cast and add to view
### Show custom drag
* You are allowed to show a custom view for a drag
* you have to give a target
* you can also animate the drag lift itself

### Custom drops
* You can add sprinloaded actions to any view
* Animations are the same as drag (different name (Drag -> Drop))
## Next steps
# What's New in iTunes Connect
### [Session 302](https://developer.apple.com/videos/play/wwdc2017/302/)
## Ratings, Reviews and Responses
* ALlowed to respond to review in iTunes Connect now
* One pairing per review for response
* Customer recieves a push and email notification when a developer responses or updates a response to a review
* Developer can opt into an e-mail notification for users updating their review
* Ratings have increased on average 1.5 Stars per review when responsed to

### Best Practices
* Be responsive
* Listen to customers, stay on topic, don't use to advertise or incentivise hire ratings
* If responding to customer concerns that are fixed in a new release, put it in the What's New section of the app update

## TestFlight
* Mutliple Build Distribution
    * Allowed to update Build Info before distro
    * Builds can be expired manually
        * Does not remove from user phones until 90 interval is reached
* Testing Groups
    * Test Groups are allowed to send feedback
* Internal testers get auto distribution
* Individual Tester Metrics
* Resend Invite to Testers
* External testers limited to 2K
    * Increase to 10K later this year

## App Store
* Xcode 9 and iOS 11 App Icons will be managed via assets in app, note iTunes Connect
    * App thinning will remove this before sending it to the user
* Subtitle is 30 characters in length
    * It will show up almost everywhere the App Name does in the App Store
* 3 App Previews per language

### Promotional Text Field
* Always editable
* Frequently changing information
* Description will be locked after app review

## Promoting In-App Purchases
* Product Page
    * Limited to 20 Promoted IAP per product
    * You can control order and visibility
* Searchable
* Trending Section for IAP
* Eligible to be promoted by the Editorial team

### How to
* Must provide some promotional ata
    * Image
    * Name
    * Description
* Will be reviewed by the app review team
* Configuration
    * Activation (Will it count against your 20 count limit)
    * Order (Where it goes)
    * Visibility (When it shows)
* StoreKit Configuration
    * `SKPaymentTransactionObserver`
        * Must be implemented or won't show up
    * `SKProductStorePromotionController`
        * Configure Ordering and Visibility

## Phased Release
#### Released over a 7 day period for automatic update users
* Day 1 = 1%
* Day 2 = 2%
* Day 3 = 5%
* Day 4 = 10%
* Day 5 = 20%
* Day 6 = 50%
* Day 7 = 100%
* New users and manual upgrades will get the latest version
* Allowed to pause automatic rollout (users can still manually update)
* Can release early if confident
* Available today, on all iOS versions

## App Review
* Make sure app functions throughout review
    * Make sure external services are working, otherwise it can delay the review
* Run on an actual device
* Sign-In information is up to date
* Use App Review notes for obscure or hard to get to places
    * Use for other information the reviewers may need as well
* Binary compatibility
* Use keywords
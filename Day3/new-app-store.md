# Introducing the New App Store
### [Session 301](https://developer.apple.com/videos/play/wwdc2017/301/)
## Today View
* Today is the area to features products in a new way
    * Helpful tips and tricks
    * App of the day and game of the day
    * Sharing curated collections, more in the spotlight than before
    * 12 Today Cards at launch
        * Order will change day to day
    * Editoral Content will be searchable

## Games
* Focus on Games exclusively
* Charts for Games and charts for Apps are different now
* Games now can have categories
* Icons are not the only focus anymore
    * Subtitles will play a significant role on the store
    * Editorials are also featured

## Apps
* Icons are not the only focus anymore
    * Subtitles will play a significant role on the store
    * Editorials are also featured
* App previews are now shown
* In-App purchases will be shown on the App Store, and have a section on the app page
    * Seperate icon needed for IAP
        * Should show what it is the user is buying
    * Different frame design to keep it obvious it's an IAP

## Clarity
* Rating, awards, ranking and age rating are more predominate
* App previews are showcased more, multiple previews allowed
* Rewiews are more prominent as well


## Editorals
### Principles
* We celebrate apps
* Voice of the App Store
* Offer independent insights
    * No advertising in the app store
* Interesting and important stories
* Deliever a fresh and unexpected takes

# Essential Design Principles
## [Session 802](https://developer.apple.com/videos/play/wwdc2017/802/)
* [Human Interface Guidelines (HIG)](https://developer.apple.com/ios/human-interface-guidelines/overview/design-principles/) updated

## Apps should provide...
* prediciability and stability
* Clear and helpful information
* Streamlined and simple workflows
* A delightful experience

### Wayfinding
* Where am I
* Where can i go
* What will I find when I get there
* Whats nearby 
* How do I get out

### Feedback
* Status
* Completion
* Warnings
* Errors

### Good Feedback answers
* what can I do 
* what just gappened
* whats happening
* what will happen in the future

Talk to people who haven't used the app!

## Visibility
* Figure out clusters of Visual data that help people make desicions without having to look in multiple places
* There are limits, you have to find a balance

## Consistency
* Represent similiar items in similar ways
* Consistency improves visibility
* Platform conventions are key
* Internal consistency 
    * Provides cohesion

## Mental Models
* Developed through personal experiences
#### System Model
* Model of how a system works (e.g. how hot/cold water work in a faucet)
#### Interaction Model
* Model with how interaction with a system works (e.g. turning faucet handles)
* When Mental Models are matched, people view the system as intuitive, likewase when Mental Models are not meant, systems are considered unintuitive
* When considering big changes to long existing features, you must be sure it's a long term win for the user, test it

## Proximity
#### Distance between a control and the object it affects
* The closer the control and object are, the more obvious it is they interact with each other

## Grouping
* Helps people understand relations of different objects

## Mapping
* Designing controls to represent the object they affect
* Relates to how controls relate to each other

## Affordance
* Physical characterisitics provide indiciation to what interactions are possible
* The relationship individuals have to an object
    * Changes from individual to individual
* i.e. Sliders provide dragging, Buttons provide a click

### Perceived Affordance
* People are more likely to perveive an affordance when it's an aation they are more likely to take

## Progessive Disclosure
* Technique for managing complexity
* Gradually eases people from the simple to the more complex
    * Helps to hide complexity by not letting users see options they wouldn't care about
* It can bury information and functionality

## 80/20 Rule
* 80% of a systems effects come from 20% of it's actions

## Symmetry
* Symmetrical components are pereived as connected even if they're not
* User Symmetry to provide a sense of balance and order
* Designing is about resolving differences
* Too much of a good thing is bad, be measured in your approach
* Size matters, it can change what the design is supposed to be
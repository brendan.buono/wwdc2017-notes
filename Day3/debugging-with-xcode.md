# Debugging with Xcode
### [Session 404](https://developer.apple.com/videos/play/wwdc2017/404/)
## Wireless debugging
* AR, VR and camera app developers
* Motion-sensing and fitness app developers
* Accessory makers
* Developer convenience
* iPhone, iPad and Apple TV supported
    * iPod,iPhone and iPad running iOS 11
    * MacOS 10.12.4
* All debugging features available
* Available over Ethernet and Wi-Fi

### How to Setup
* Open up Device Management
* Select connect via network
* Home and small buiness networks should auto work
* Direct by IP Address available for more complex networks

### Support
* Attaching to a remote process wirelessly
* All debugging information avaiable
* Tracking pinning in instruments

## Breakpoints
* Breakpoint editor text fields have code completion
* Tooltip shows info about the breakpoints
* Breakpoint searching does a deep search on all text fields

## `ViewController` debugging
* `ViewController`'s are now included in the ViewHierarchy

## SpriteKit Debugging
* First class citizen in the view debugger
* Can explode Sprites into the View Hierarchy

## SceneKit Debugging
* Partial support
* SceneKit editor and Debugging combined
    * Takes a snapshot of the SceneKit scene
    * Only modifying in memory snapshot
    * Can be exported
* Supported cleints are iOS 11=< and High Sierra

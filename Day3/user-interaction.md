# Modern User Interaction on iOS
#### Mastering the UIKit `UIGestureRecognizer` System
### [Session 219](https://developer.apple.com/videos/play/wwdc2017/219/)

## Building Blocks
* Pay attention to cancel events, otherwise touches will be slow
* Drag and Drop makes correct handling even more important

## System Gesture Interaction
* Swipe from Bottom - Dock
* Swipe from Side - Slide Over
* Swipe from Top - Cover Sheet
* Tap, Pinch, Rotate and Long Press from App take over System
* Pan, Swipe and Responders have to be inferred by the system
* iOS 11 allows telling the system whether or not to defer system gestures
    * `override func preferredScrenEdgesDeferringSystemGestures () -> UIRectEdge`
#### Don't do it
* Thinking hard and long for when to do it
* Users are already familiar with the System interactions
* Swipe and Pan are the only reasons to do this
* App should be extremely immersive (i.e. games) to consider doing this

## Playing Nice with Drag and Drop
* `UILongPressGestureRegconizers` are now delayed
    * Long press and hold
    * Begging drag will cancel the touch
    * In the compat trait environment, Long presses are delayed until the touch ends
        * You need to long press and lift
* Examine your existing actions
    * Do they make sense with UIDragInteraction
* Present modal UI carefully next to `UIDragInteraction`
* Handle the `.cancelled` state
* Your app is interactive during a drag!
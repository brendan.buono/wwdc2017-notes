# Design for Everyone
### [Session 806](https://developer.apple.com/videos/play/wwdc2017/806)
* 1 in 7 people have a disability
* Making Apps Accessible

## Simplicity
* Easy to navigate
* Quick to get started
* Consistent Behavior
    * App UI Design Resources help with this
    * Commonality between apps allow you to learn one app, then feel more comfortable in others

## Perceivability
* 285 Million people in the world have moderate to severe vision loss
* 360 Million people in the world have hearing loss
* Maximize Legibility

### Maximize legiility
* Make as much text dynamic as possible
* Use as much of the screen wifth for text
* Display the same amount of text as the default UI
* Scale necssary content glyphs
* Captaions are great for things like videos

### Audible Cues
* VoiceOver allows users with extremely low or no vision to use the app
* Providing sounds during actions provides a pleasant experiences for users that can see the app, and those using VoiceOver to have a secondary sound  to let them know what is going on
* Siri is a great way to get started with this

### Haptic feedback
* Provides critical thresholds for actions with swipe to refresh, add, or delete.

## Integrity
### Take responsibility
* It's challenging
* Designing for everyone is a process
* The ability to recgonize as many of the constraints as possible

### Be empathetic

### Unlock potential
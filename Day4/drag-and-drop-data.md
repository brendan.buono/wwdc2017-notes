# Data Delivery in Drag and Drop
[Session 227](https://developer.apple.com/videos/play/wwdc2017/227/)
## NSItemProviders
* `itemProvider.canLoadObject(ofClass: .self)` will tell you if the item is that type of object
* `itemProvider.loadObject(ofClass: self)` loads the object
* To update the UI you must use the main thread `DispatchQueue.main.async{...}`

## Progress and Cancellation
* progress object provided
    * `let progress = itemProvider.loadObject(..)`
    * `progress.fractionCompleted`
    * `progress.isFinished`
Can you key/value observation to update UI with this data

## Maximize Compatibility
* One `NSItemProvider = one "thing" being dragged
* Multiple Representations
* Data types are defined in `MobileCoreServices`

### Fidelity Order
* Highest fidelity first
    * Internal type
    * Highest fidelity common type
    * Next highest fidelity common type
    * etc...
* Use concrete data types when possible

## Model Classes

### `NSItemProviderReading` and `NSItemProviderWriting` Protocols
* Writing export data from model object
    * Implement data type allowed and converter3
* Reading creates model object from data
* Maintain model classes, not UI

### Summary
* Conform to `NSProviderReading`,`NSProviderWriting` protocols
* Conforms to `NSObject`
* Adds support for Drag and Drop, UIPasteConfiguration, and Pasteboard

## Advanced Topics
### Data Marshaling
* Can provide data as
    * NSData
    * As file or folder
    * From a place
* Can receive data as
    * Copy NSData
    * Copy file or folder
    * Attempt to open in place
* All options are interchangable, can mix and match from send and receive

### Progress and Cancellation (Provider)
* Return your own `Progress`

### Per-Representation Visibility
* Restrict visibility of each type of data
    * Same application
    * Same team
    * Everyone
* Use to hide private types

### Team Data
* `teamData` property
* Tp to 8 KB of metata
* Visiable to same Team
* Available before full data

### Suggested Name
* `suggestName` property
* Used as file name

### Prefered Presentation Size
* USe to target drop animation
* Can tell the receiver what size to expect

### File Providers
* App extension
* Allows app to be terminated
* Allows download from `URL` even if original app is terminated
* If a URL is provided in a File Provider container, it will continue
* File is kept in place against mutliple apps opening it
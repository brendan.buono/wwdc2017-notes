# What's new in LLVM
[Session 411](https://developer.apple.com/videos/play/wwdc2017/411)
## Using avaiable APIs
* Swift can check with query syntax with `#available`
* Swift availability checking is now available in Objective-C
* Compiler will warn against unavailable APIs
* Can use `if(@available(iOS 11,*))`
    * Returns true when iOS 11 APIs are available
    * Star is required, says available on all other platforms
* `- (void) showFaces API_AVAILABILITY(ios(11.0))` to declare function only available in specific OS's
    * Available for classes as well

### How will this work
* Will only check against iOS 11, tvOS 11, macOS 10.13 and watch OS 4
* Old API's will not be checked
* No code needs to change
* New projects will need to check against all versions
* Existing projects can opt in with `Unguarded checking` in the project settings to check all versions

## Find Deep Bugs - New Analyzers
* Do not compare number objects to scalars
* Do Not Implicitly Convert Number Objects to Booleans
* Level of the check can be set in Build Settings
    * Yes (Aggressive) means the compiler will warn even when it's unsure, but thinks you're being ambigious
* Use `dispatch_once()` to initialize Global State
    * Guarentees block is called exactly once
    Predicate must be a global or static variable
* Do not store ` dispatch_once_t` in instance variables
* Do Not Auto-Sythesize `NSMutable` copy Properties

#### Run Analyzers on your Code!
* Analyze during Build is an option

# Design Studio Shorts 2
### [Session 805](https://developer.apple.com/videos/play/wwdc2017/805)
## What's new in iOS
### Wayfinding
* Large Titles
    * Best places to use
        * Top Level of an App with Tabs
        * Apps With Rich and Extensive Content
        * Views with Visually Similar Layouts
    * Use Large Titles purposefully

### Typographic Hierarchy
* Test positioning conveys importantance
    * Size, Weight and Color also help with this

### Contrast
* Use Subtle emphasis to bring attention to navigation or action items
    * i.e. Try Again screen highlights the buttons
    * Nav bar has medium weight instead of regular
        * iPad, Landscape and Portriat mode may have different configurations as well

## Size Classes
### Size Classes
* All devices are `compact` or `regular`
* This for for height and width independently

### Dynamic Type
* Textiles defined for the different sizes
    * Names are meant to be descriptive of where things go

### UIKit Elements
* Apple UI Design Resource has items that implement the design features above automatically when used
* Start design for iPhone 7, then go to iPad, think about split view

## Rich Notifications
* Be mindful of people's time
* Too many notifications will cause people to shut them off, or even delete the app
* Users should not always have to enter the app to take action on the notification
    * Short Look - What is the notification about
    * Long Look - More detail to describe the info in the notification
    * Quick Actions - Relevant actions to the info in the notification


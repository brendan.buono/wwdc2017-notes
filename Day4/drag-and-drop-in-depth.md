# Drag and Drop in Depth
### [Session 223](https://developer.apple.com/videos/play/wwdc2017/223/)

## First ten minutes I missed

## Drop Proposal
* How you want to handle the drop
* `UICollectionViewDropProposal`, `UITableViewDropProposal`
    * Has all of the same states as regular UIDropView
    * Also has a Drop Intent
* Drop Intent allows for preview in the collection or table view while dragging items over said view
* Providing a Drop Proposal `func collectionView(... dropSessionDidUpdate... withDestinationIndexPath)...`
* `destinationIndexPath`
    * Can be `nil` if not over a cell
    * If at the end of the collection, will be equal to count

## Drop Animations
### Drop to an item row
* Set up animations using the drop coordinator
* `collectionView(...performDropWith...)`
* `coordinator.drop`

### Drop into an item/row
* Can rescale view to fit inside drop are for animation
    * Pass in a rect to resize the view
* Drop to an item/roe
* Drop into an item/row
* Animation to any location with a transform

## Placeholders
* Temporary insertions in the collection/table view
* You provide the cell, we wdo the bookkeeping for you
* `coordinator.drop ... toPlaceholderInsertedAt...`
* Avoid `reloadData`, use `performBatchUpdates(_:completion:)` instead
    * `reloadData` will reload the entire view
    * `var hasUncommitedUpdates: Bool` will you you know if the collection has uncommited updates

## Supporting reordering
* Implement dropDelegate
    * operation `.move` and intent of `.intentAtDestinationIndexPath`
* `UITableView`
    * `func tableView(... moveRowAt...)` still supported
        * Use instead of `tableView(...performDropWith...)`
* `UICollectionView`
    * `collectionView(... performDropWith...)`
    * Will receive `originalIndexPath` when coming from the same collectionview
* `reorderingCadence` allows for some control of the drop speed
    * `.immediate`, `.fast`, `.slow`

## Spring Loading
* `UISpringLoadedINteractionSupporting`
* Customize spring loading with optional delegate method
    * `collectionView(shouldSpringLoadItemAt...)`

## Customize Cell Appearance
* `.none`, `.lifting`, `.dragging`
* `dragStateDidChange(_:)` allows for customization
* By default entire cell is used for drag preview
* delegate has a customization method, you can provide a bezier to clip

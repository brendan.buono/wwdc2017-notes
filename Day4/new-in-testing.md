# What's New in Testing
### [Session 409](https://developer.apple.com/videos/play/wwdc2017/409)
## Enhancements
 * Block based test teardown
 * `XCUIElement.waitForExistence()`
 * xcodebuild supports parallel device testing

## Testing Asynchronous Operations
### `XCTWaiter`
* Extracted logic from `XCTestCase`
* Explpicit list of expectations
* Calls bcack to `XCTWaiterDelegate`
* `let result  = XCTWaiter.wait(for:[], timeout: 10)`
### `XCTestExpectation`
* Decoupled from XCTestCase
* Multiple fulfillments
* Inverted Behavior
* Allowed to enforce order of expectations

## Multi-App Testing
* App groups
* Automate Settings
* Automate App Extensions

## UI testing performance
* Queries use accessibility data
 1. Optimizations were made in terms of doing queries remotely
 2. Another optimization was analyzing the queries to have to look at less in a snapshot
 3. Eliminate Snapshots completely (First Match)

### First Match vs. Match All
 * Match All detects ambiguity
 * First match requires precision
 
#### Examples
>  `app.buttons.firstMatch`

* Not a great idea because theres a lot of buttons

> `app.buttons["MyButton"].firstMatch`

* Better because there is an identifier, but

> `app.navigationBars["MyButton"].firstMatch`

* Best use, limiting scope to the navigation bars and looking for an identifier

### Block Based Prediciates
 * If you find a reason to use these, file an enhancement feature!

## Activities, Attachments, and Screenshots
### Activites
* Create structure for long running tests

 > `XCTContext.createActivity(named: "Draft Player"){...}`

### Attachments
 * Data from tests
 * Improved triage
 * Post run review

### Screenshots
* `XCUIScreenshotProviding`
* Implemented by `XCUIElement.screenshot` and `XCUIScreen.screenshot`

#### Attachment Lifetime Policies
* Delete screenshots if tests pass
* Hold on to if tests fail
* Scheme option available
* There is also a `XCTAttachment` API for case by case configuration
# Engineering for Testability
### [Session 414](https://developer.apple.com/videos/play/wwdc2017/414/)

## Testable App Code

### Structure of a Unit Test
* Prepare inout
* Run the code being tested
* Assert it ran correctly

### Characteristics of Testable Code
* Control over inputs
* Visibility into outputs
* No hidden state

## Testability Techniques
### Protocols and parameterization
* You can use extensions and protocols and help with tests

> ` protocol URLOpener { func bool canOpenURL(url: URL) }`

> ` extension UIApplication: URLOpener {}`

* Reduce referecnes to shared instances
* Accept parameterized input
* Introduce a protocol
* Mock out protocol for test implmentation

### Seperating Logic and Effects
* Refactor code out of classes to enscapsulate logic to be able to test
    * i.e. a `CleanupPolicy` for a cache system
    * Instead of have a `clearCache` function, you can have a `CleanupPolicy` protocol that has an implementation, which is isolated from any other dependencies
* Extract algorithms
* Functional style with value types
* Thin layer on top to execute effects
    * Good candidate for integration testing

## Scalable Test Suite
### Balance between UI and Unit Tests
* Pyramid
    * UI - Top
    * Integration - Middle
    * Unit - Bottom
* Unit tests are great for testing small, hard to reach code paths
* UI test are better to test broader, end to end code paths

## Writing Code to help UI tests scale
#### Abstract UIElement Queires
* Store parts of the query in a variable
* Wrap complex queries in utility methods
* Reduces noise and clutter in UI test

#### Creating objects and Utility Functions
* Encapsulate common testing workflows
* Cross-platform code sharing
* Improves maintainability

### Use Keyboard Shortcuts
* Use shortcuts to make your code more readable
    * ` app.typeKey("c",modifierFlags[.command,.shift])`
* Make code more compact

## Quality of Test Code
### **Treat your test code with the same amount of care as your app code**
* Important to cosider even though it isn't shipping
* Test code should support the evolution of your app
* Coding principles in app code also apple to your test code
* Code reviews for test code, not code reviews with test code

### App Code and Test Code aren't different, they're just our code and should be treated as code
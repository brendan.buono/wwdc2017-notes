# App Startup Time: Past, Present, and Future
### [Session 413](https://developer.apple.com/videos/play/wwdc2017/413)
#### Terminology
* Startup Time
    * Time spent before `main()` is called
* Launch Closure
    * All the information necessary to launch an application

## Review of the app launch startup advice
* Do less!
    * The less you do, the faster apps can launch
    * Use fewer dylibs (third party)
    * Declare fewer classes/methods
    * Use fewer initiliazers
    * The above applies to Objective-C, so use Swift!
        * No initializers
        * Swift 3.2 and 4 have size improvements that help

## New tools to help find slow initializers
### Static Initilizer Tracing
* Available in iOS 11 and High Sierra
* Provides precise timing for each static initializer

## Brief histroy of dyld
### dyld 1.0 (1996-2004)
* Shipped in NeXTStep 3.3
* Predated POSIX dlopen() standardized
* Before most system used large C++ dynamic libraries
* Prebinding added in macOS Cheetach(10.0)

### dyld 2.0 (2004-2007)
* Shipped in macOS Tiger
* Complete rewrite
* Full native dlopen()/dlsym() semantics
* Designed for speed
    * Limited sanity checking
    * Security issues
* Reduced prebinding

### dyld 2.x (2007-2017)
* More archtectures and platforms
    * x86,x86_64, arm64
    * iOS, tvOS, watchOS
* Improved Security
    * Codesigning, ASLR, bounds checking
* Improved Performance
    * Prebinding completely eliminated, replaced by Shared Cache

##### Shared Cache
* Introduced in iOS 3.1 and macOS Snow Leopard
* Single file that contains most system dylibs
    * Rearrages binaries to imprve load spped
    * Pre-links dylibs
    * Pre-builds data structures used fyld and OvjC
* Built locally on macOS, shipped with iOS, tvOS, and watchOS

## The all new dyld is coming in this years Apple OS Platform - dyld 3 (2017)
* Complete rethink of dynamic linking
* On by default for most macOS Systems this week

### Why
* Performance
    * Whhat is the minimum amount of work we can do to start an app
* Security
    * Can we have more aggressive secuirty checks?
* Reliabiity
    * Can we design somethn that is easier to test?

### How
#### Move complex operations out of process
* Most of dyld is now a regular daemon

#### Make the rest of fyld as small as possible
* Reduce attach surface
* Speeds up launch
    * Removes code that is not needed
    * Code that doesn't need to execute often

#### Identify security sensitive components
* Bounds checking
* @rpath confusion attacks

# Identify components that are cache-able
* Dependencies don't change between launches

### Architecture
* An out of process MachO parser
* In process engine that runs launch closures
* launch clousre caching service
    * System app launch closures built into shared cache
    * Third party app launch closure built during install
        * Rebuilt during software updates
    * On macOS the in process engine can call out to a daemon if necessary on first launch

### Potential Issues
* Fully compatible with dyld 2.x
    * Some existing APIs disable dyld 3's optimizations or require slow fallback paths
    * Some existing optimizations done for dyld 2.x no longer have any impact
* Stricter linking semantics
    * Workarounds for old binaries
    * New binaries will cause linker errors

#### Unalgined pointers in __DATA
* Unaligned pointers in the struct will be embedded in the __DATA segment
* Dixing up unalgined pointers in more complex
    * Can span mutliple pages
    * Can have atomicity issues
* Static linker already emites a warning
* *Not a problem in Swift*

#### Eager symbol resolution
* dyld 2 perform lazy symbol resolution
    * Symbol lookups are too expensive to do them up front
    * Each symbol is looked up the first time you call it
    * Missing symbols cause a crash the first time they are called
* App built against current SDKs will run with unknown symbols
    * Identical behavior to dyld 2, on first call it will crash
* Apps build against future SDKs will fail to launch now

#### `dlopen()`/`dksym()`/`dladdr()`
* Still have problematic semantics
    * Still necessary in some cases
* Symbols found with `dlsym()` must be found at runtime
    * Cannot be pre-linked by dyld 3
* Will be slower in dyld 3, use with caution
    * Apple is working on alternatives

#### `dlclose()`
* Misomer
    * Decrements a refcount, doe  not meccssarily close the fylib
    * Not appropriate for resource manager
* Features that prevent a dylib from unloading
    * ObjC classes
    * Swift classes
    * C__thread and C++ thread_local variables
* Consider making `dlclose()` a no-op everywhere except macOS

#### all_image_infos
* Interace for introspecting fylibs in a process
    * Struct in memroy
    * Wastes a lot of memmroy
    * Going away in future releases
    * Please give feedback to how its being used

* Make sure your app launches with built with `-bind_at_load` to LD-FLAGS **DEBUG ONLY**
* Fix any unaligned pointers in your app's __DTA segment
* Make sure you are not depending on terminators running when you call`dlclose()`
# Working with HEIF and HEVC
### [Session 511](https://developer.apple.com/videos/play/wwdc2017/511)
#### HEVC is the successor to H.264
## Why
* H.264 has reached the limit of what it can do

## Access
* PhotoKit will deliver HEVC assets for playback
    * Should "Just work"
    
## Playback
* Supported in modern media frameworks
* Steaming, play-while-download, and local file are supported
* No API Opt-In required

### Minimum Configuration
* Hardware Decode
    * A9 chips and above
* Software Decode
    * Everyone

### Playback Availability
* No all system support scrubbing, 2x play, etc

## Capture
* Capture Supported in AVFoundation
* Only available on A10 Chip

## Export
* Transcoe to HEVC with AVFoundation and VideoToolbox
* MPEG-4 and QuickTime outputs supported
### Support
* 8-bit Hardware Encode on A10 Chip
* 10-bit all Software Encoding

## HEIF
### Why
* Better compression
* Depth availability is much better
    * Allows much better access for filters and such
* Supports industry standards for metdata
* Supports arbitrarily large files
* Dynamically loads pixels when needed for better experience
* Compression is much better

### Extensions
* `.heic` = HEVC Image
* (There are more the slide moved too quickly)

# Teaching with Swift Playgrounds
### [Session 416](https://developer.apple.com/videos/play/wwdc2017/416/)

## Goal
### Teaching
* What is your goal? More importantly, what is your student's goal?
    * Many students will ask, when will I use this?!
* People want to create
    * Let them make a mess!
* At the beginning, start with a question

### Design
* Goals for a student
    * Complete a task
    * Experiment
    * Practice a new skill
        * Users won't learn new ideas immediately! Help them with repetition
    * Think about a concept
    * Create

#### Prose Tools
##### Glossary Entries
* Glossary entries help students learn about domain specific words they may not have known beforehand
    * Put text in front of people to see what words need glossary entries!

##### Diagrams
* Helpful in addition to definitions to give a visual representation of a concept for a user

##### Callouts
* Students skip stuff, giving callouts will help the student zone into highlights on what they're learning

##### Shortcut Bar
* Only include relevant choices
* Use concise method names
* Provide common keywords
    * Ancillary thought distracts from learning, so providing common keywords allows the student to focus on what you want to teach them

## Passion
### Teaching
* Bring your passion for programming into your lessons
    * You want your students to be as engaged as your are

## Audience
### Teaching
* What is your audience?
    * You need to know your audience to build your content

##### Details
* Don't overwhelm your student with details
* Don't start with the specifics, give them something fun to play with first and ease them into the more complex concepts

## Designing Swift Playgrounds
* Cut scenes help keep the learner engaged
* Tips help keep the student engaged as well
* You can add solutions to a Playground as well
    * Extremely helpful for learners who become stuck
* Assessments can be added at the end
* Iterate while designing!
    * Cheap, fast prototype will help you build a great Playground and get feedback quickly
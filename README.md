# WWDC Notes
## Day 2
* [Drap and Drop](./Day2/drag-and-drop.md)
* [Localizing in XCode 9](./Day2/localizing-in-xcode-9.md)
* [What's new in Swift](./Day2/whats-new-in-swift.md)
* [Updating Apps to iOS 11](./Day2/updating-app-to-ios11.md)
* [Introducting Drag and Drop](./Day2/drag-and-drop.md)
* [Introducing ARKit](./Day2/ARKit.md)

## Day 3
* [Debugging with Xcode 9](./Day3/debugging-with-xcode.md)
* [Introducing the New App Store](./Day3/new-app-store.md)
* [Essential Design Principles](./Day3/essential-design-principles.md)
* [Modern User Interaction on iOS](./Day3/user-interaction.md)
* [What's new in iTunes Connect](./Day3/itunes-connect.md)

## Day 4
* [Drag and Drop in Depth](./Day4/drag-and-drop-in-depth.md)
* [Data Delievery in Drag and Drop](./Day4/drag-and-drop-data.md)
* [Design for Everyone](./Day4/design-for-everyone.md)
* [Design Shorts 2](./Day4/design-shorts-2.md)
* [What's New in Testing](./Day4/new-in-testing.md)
* [What's New in LLVM](./Day4/new-in-llvm.md)

## Day 5
* [App Startup Time: Past, Present, and Future](./Day5/app-startup-time.md)
* [Working with HEIF and HEVC](./Day5/heif-and-hevc.md)
* [Engineering for Testability](./Day5/engineering-for-test.md)
* [Teaching with Swift Playgrounds](./day5/teaching-with-swift-playgrounds.md)